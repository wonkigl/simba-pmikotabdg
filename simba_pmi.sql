-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2017 at 02:30 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simba_pmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE `log_activity` (
  `id_log` int(11) NOT NULL,
  `username_pengguna` varchar(10) DEFAULT NULL,
  `tgl_transaksi` date DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `modules` varchar(50) DEFAULT NULL,
  `waktu_transaksi` varchar(10) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `id_pengguna` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini berfungsi untuk menampung data log aktivitas-aktivitas yang telah dilakukan pengguna';

-- --------------------------------------------------------

--
-- Table structure for table `m_assessor`
--

CREATE TABLE `m_assessor` (
  `id_assessor` int(11) NOT NULL,
  `tgl_lahir_assessor` date NOT NULL,
  `alamat_assessor` varchar(50) NOT NULL,
  `nohp_assessor` varchar(15) NOT NULL,
  `tempat_lahir_assessor` varchar(50) NOT NULL,
  `nama_assessor` int(11) DEFAULT NULL,
  `gender_assessor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini berfungsi menampung data diri assessor\r\n';

-- --------------------------------------------------------

--
-- Table structure for table `ref_kecamatan`
--

CREATE TABLE `ref_kecamatan` (
  `id_kecamatan` int(5) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `kode_kecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini berfungsi untuk menampung data kecamatan sebagai referensi\r\n';

-- --------------------------------------------------------

--
-- Table structure for table `ref_kelurahan`
--

CREATE TABLE `ref_kelurahan` (
  `id_kelurahan` int(5) NOT NULL,
  `nama_kelurahan` varchar(50) NOT NULL,
  `kode_kelurahan` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini berfungsi menampung data kelurahan (sebagai referensi)';

-- --------------------------------------------------------

--
-- Table structure for table `sis_pengguna`
--

CREATE TABLE `sis_pengguna` (
  `id_pengguna` int(2) NOT NULL,
  `username_pengguna` varchar(10) NOT NULL,
  `password_pengguna` varchar(10) NOT NULL,
  `nama_lengkap_pengguna` varchar(20) DEFAULT NULL,
  `email_pengguna` varchar(50) DEFAULT NULL,
  `telepon_pengguna` varchar(15) DEFAULT NULL,
  `tipe_pengguna` enum('1','2','3') DEFAULT NULL,
  `waktu_registrasi` date DEFAULT NULL,
  `waktu_update` date DEFAULT NULL,
  `input_oleh` varchar(50) DEFAULT NULL,
  `update_oleh` varchar(50) DEFAULT NULL,
  `status_aktif` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini berfungsi menampung data para pengguna sistem berdasarkan peran atau role yang ditetapkan';

-- --------------------------------------------------------

--
-- Table structure for table `tr_assessment_bagakhir`
--

CREATE TABLE `tr_assessment_bagakhir` (
  `id_assessment_bagakhir` int(5) NOT NULL,
  `kdoe_transaksi` char(6) NOT NULL,
  `ket_situasi_keamanan` varchar(50) DEFAULT NULL,
  `kebutuhan_mendesak` varchar(50) DEFAULT NULL,
  `tindakan_pmi` varchar(50) DEFAULT NULL,
  `tindakan_pemerintah` varchar(50) DEFAULT NULL,
  `tindakan_lsm` varchar(50) DEFAULT NULL,
  `tindakan_ormas` varchar(50) DEFAULT NULL,
  `kontak_person` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_assessment_bagawal`
--

CREATE TABLE `tr_assessment_bagawal` (
  `id_assessment_bagawal` int(5) NOT NULL,
  `kode_transaksi` char(6) NOT NULL,
  `tgl_assessment` date NOT NULL,
  `waktu_assessment` time DEFAULT NULL,
  `kode_kelurahan` int(5) DEFAULT NULL,
  `kode_kecamatan` int(5) DEFAULT NULL,
  `kode_propinsi` int(5) DEFAULT NULL,
  `id_assessor` int(5) DEFAULT NULL,
  `id_jenis_kejadian` int(5) DEFAULT NULL,
  `jml_meninggal_dunia` int(5) DEFAULT NULL,
  `jml_luka_berat` int(5) DEFAULT NULL,
  `jml_luka_ringan` int(5) DEFAULT NULL,
  `status_pengungsi_idp` enum('Y','N') DEFAULT NULL,
  `lokasi_pengungsian` varchar(50) DEFAULT NULL,
  `jml_pengungsi_idp` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini menampung data transaksi ketika proses input hasil assessment dilakukan (bagian umum dan informasi umum)';

-- --------------------------------------------------------

--
-- Table structure for table `tr_assessment_bagtengah`
--

CREATE TABLE `tr_assessment_bagtengah` (
  `id_assessment_bagtengah` int(5) NOT NULL,
  `kode_transaksi` char(6) NOT NULL,
  `jml_rumah_tinggal` int(5) DEFAULT NULL,
  `jml_rusak_berat` int(5) DEFAULT NULL,
  `jml_rusak_tingan` int(5) DEFAULT NULL,
  `akses_jalan` enum('Y','N') DEFAULT NULL,
  `akses_telepon_selular` enum('Y','N') DEFAULT NULL,
  `akses_kantor_pos` enum('Y','N') DEFAULT NULL,
  `akses_internet` enum('Y','N') DEFAULT NULL,
  `akses_kendaraan_umum` enum('Y','N') DEFAULT NULL,
  `akses_jembatan` enum('Y','N') DEFAULT NULL,
  `sarum_listrik` enum('Y','N') DEFAULT NULL,
  `sarum_air` enum('Y','N') DEFAULT NULL,
  `sarum_sekolah` enum('Y','N') DEFAULT NULL,
  `sarum_tempat_ibadah` enum('Y','N') DEFAULT NULL,
  `sarum_rs` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini menampung data transaksi ketika proses input hasil assessment dilakukan (sub bagian dampak sarana & prasarana, situasi keamanan dst)\r\n';

-- --------------------------------------------------------

--
-- Table structure for table `tr_foto`
--

CREATE TABLE `tr_foto` (
  `id_foto` int(11) NOT NULL,
  `latitude` decimal(12,8) NOT NULL,
  `longitude` decimal(12,8) NOT NULL,
  `is_primary` smallint(6) DEFAULT NULL,
  `directory` varchar(255) NOT NULL,
  `kode_transaksi` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table ini berfungsi untuk menampung data foto hasil assessment yang dicapture oleh assessor';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `m_assessor`
--
ALTER TABLE `m_assessor`
  ADD PRIMARY KEY (`id_assessor`);

--
-- Indexes for table `ref_kecamatan`
--
ALTER TABLE `ref_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `sis_pengguna`
--
ALTER TABLE `sis_pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `tr_assessment_bagakhir`
--
ALTER TABLE `tr_assessment_bagakhir`
  ADD PRIMARY KEY (`id_assessment_bagakhir`);

--
-- Indexes for table `tr_assessment_bagawal`
--
ALTER TABLE `tr_assessment_bagawal`
  ADD PRIMARY KEY (`id_assessment_bagawal`);

--
-- Indexes for table `tr_assessment_bagtengah`
--
ALTER TABLE `tr_assessment_bagtengah`
  ADD PRIMARY KEY (`id_assessment_bagtengah`);

--
-- Indexes for table `tr_foto`
--
ALTER TABLE `tr_foto`
  ADD PRIMARY KEY (`id_foto`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
